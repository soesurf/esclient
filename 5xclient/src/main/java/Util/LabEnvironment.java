package Util;

/**
 * Created by surfer on 2017/07/08.
 */
public class LabEnvironment {

    public String cluster_name;
    public String hostname;

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getCluster_name() {
        return cluster_name;
    }

    public void setCluster_name(String cluster_name) {
        this.cluster_name = cluster_name;
    }


    public LabEnvironment(String hostname, String cluster_name) {
        this.hostname = hostname;
        this.cluster_name = cluster_name;
    }


}
