import Util.EsClient;
import Util.LabEnvironment;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.bulk.byscroll.BulkByScrollResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.ingest.PutPipelineRequest;
import org.elasticsearch.action.ingest.WritePipelineResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.sort.SortOrder;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.time.LocalDate;


import static org.elasticsearch.common.xcontent.XContentFactory.*;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;



public class Main {

    public static LabEnvironment env = new LabEnvironment("localhost", "elasticsearch");


    public static Client client = EsClient.GetClient(env, true);



    public static String index_name = "hotelg";
    public static String type_name = "type1";
    public static String method_name = "PUT";
    public static String host_name = "localhost";
    public static int transport_port = 9300;
    public static String cluster_name = "elasticsearch";




    public enum FoodMenu {
        Sushi, Pizza, Ramen, Tonkatsu, Yakiniku, Pasta,
        Salad, Udon, Somen, Soba, Inari, Burger, Yakitori, Tofu, Sakana;

        private static final List<FoodMenu> VALUES =
                Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static FoodMenu randomFoodMenu()  {
            return VALUES.get(RANDOM.nextInt(SIZE));
        }
    }



    public enum Month {
        JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE,
        JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER;

        private static final List<Month> VALUES =
                Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static Month randomMonth()  {
            return VALUES.get(RANDOM.nextInt(SIZE));
        }
    }

    public enum Day {
        SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, RRIDAY, SATURDAY;


        private static final List<Day> VALUES =
                Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static Day randomDay()  {
            return VALUES.get(RANDOM.nextInt(SIZE));
        }
    }

    public enum Country {
        JAPAN, FRANCE, INDIA, CANADA, CHILI, BRAZIL, HAWAII;


        private static final List<Country> VALUES =
                Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static Country randomLocation()  {
            return VALUES.get(RANDOM.nextInt(SIZE));
        }
    }

    public enum JapanCity {
        Tokyo, Kyoto, Osaka, Nara;

        private static final List<JapanCity> VALUES =
                Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static JapanCity randomJapanCity()  {
            return VALUES.get(RANDOM.nextInt(SIZE));
        }
    }

    public enum Group {
        G1, G2, G3, G4, G5;


        private static final List<Group> VALUES =
                Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static Group randomGroup()  {
            return VALUES.get(RANDOM.nextInt(SIZE));
        }
    }


    public static void main(String[] args) throws Exception {



        getDateDiff();
//        Thread.sleep( 3000  );
//        CreatePipeline();
//        CreatePipelineforAttachment();
        AddDocuments();
//        AggregationSample4();
//        GetDocuments();

//        Delete_By_Query();

        client.close();

    }


//
//    private static Client GetClient(boolean has_xpack) {
//
//
//        String user_password = "elastic:changeme";
//        Settings.Builder builder = Settings.builder();
//
//        builder.put("cluster.name", cluster_name);
//        if (has_xpack)
//            builder.put("xpack.security.user", user_password);
//
//
//        try {
//            TransportAddress transportAddresses = new InetSocketTransportAddress(InetAddress.getByName(host_name),transport_port);
//
//            if (has_xpack) {
//                return new PreBuiltXPackTransportClient(builder.build())
//                        .addTransportAddress(transportAddresses);
//            } else {
//                return new PreBuiltTransportClient(builder.build())
//                        .addTransportAddress(transportAddresses);
//            }
//
//
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//
//        } finally {
//
//        }
//
//        return null;
//
//
//    }


    /** This returns multiple aggregation and select source found in included_source **/
    public static void AggregationSample1() {


        String[] included_source = {"hotel_name", "daily_rate", "has_parking"};


        SearchResponse response = client.prepareSearch(index_name)
                .setSize(2)
                .setFetchSource(included_source,null)
                .setQuery(matchAllQuery())
                .addAggregation(AggregationBuilders.terms("location").field("location.keyword"))
                .addAggregation(AggregationBuilders.terms("busy_month").field("busy_month.keyword"))
                .addAggregation(AggregationBuilders.terms("closed").field("closed.keyword"))
                .addAggregation(AggregationBuilders.max("max_price").field("daily_rate"))
                .execute().actionGet();

        System.out.println(response);


    }

    /** This returns nested aggregation **/
    public static void AggregationSample2() {


        String[] included_source = {"hotel_name", "daily_rate", "has_parking"};


        SearchResponse response = client.prepareSearch(index_name)
                .setSize(2)
                .setFetchSource(included_source,null)
                .setQuery(matchAllQuery())
                .addAggregation(AggregationBuilders.terms("location").field("location.keyword")
                 .subAggregation(AggregationBuilders.terms("busy_month").field("busy_month.keyword")))
                .addAggregation(AggregationBuilders.terms("closed").field("closed.keyword"))
                .addAggregation(AggregationBuilders.max("max_price").field("daily_rate"))
                .execute().actionGet();

        System.out.println(response);


    }

    /** This returns nested aggregation with multiple order **/
    public static void AggregationSample3() {


        SearchResponse response = client.prepareSearch(index_name)
                .setSize(10)
                .setQuery(matchAllQuery())
                .addAggregation(AggregationBuilders.terms("location").field("location.keyword"))
                .addAggregation(AggregationBuilders.terms("busy_month").field("busy_month.keyword"))
                .addAggregation(AggregationBuilders.terms("closed").field("closed.keyword"))
                .addAggregation(AggregationBuilders.max("max_price").field("daily_rate"))
                .addSort("daily_rate",SortOrder.DESC)
                .addSort("star", SortOrder.DESC)
                .execute().actionGet();


        System.out.println(response);


    }



    /** This returns nested aggregation with multiple order
     * also matches multiple terms**/
    public static void AggregationSample4() {


        TermsQueryBuilder nice_query = QueryBuilders.termsQuery("group.keyword", "G1", "G2", "G3");

        SearchResponse response = client.prepareSearch(index_name)
                .setSize(10)
                .setQuery(nice_query)
                .addAggregation(AggregationBuilders.terms("location").field("location.keyword"))
                .addAggregation(AggregationBuilders.terms("busy_month").field("busy_month.keyword"))
                .addAggregation(AggregationBuilders.terms("closed").field("closed.keyword"))
                .addAggregation(AggregationBuilders.max("max_price").field("daily_rate"))
                .addSort("daily_rate",SortOrder.DESC)
                .addSort("star", SortOrder.DESC)
                .execute().actionGet();


        System.out.println(response);


    }

    public static void CreatePipeline() throws Exception {


        XContentBuilder source = jsonBuilder()
                .startObject()
                .field("description", "myt_pipeline")
                .startArray("processors")
                .startObject()
                .startObject("set")
                .field("field", "haruto")
                .field("value", "cute")
                .endObject()
                .endObject()
                .endArray()
                .endObject();

        // it is useful to declare source as XContentBuilder for debugging purpose.
//        System.out.println(source.string());

        String pipeline_name = "sample attachment";

        PutPipelineRequest putPipelineRequest = new PutPipelineRequest(pipeline_name, source.bytes(), XContentType.JSON);
        WritePipelineResponse response = client.admin().cluster().putPipeline(putPipelineRequest).get();
        if (response.isAcknowledged())
            System.out.println(pipeline_name + " is created");



    }


    public static void CreatePipelineforAttachment() throws Exception {


        XContentBuilder source = jsonBuilder()
                .startObject()
                .field("description", "file contents attachment plugin")
                .startArray("processors")
                .startObject()
                .startObject("attachment")
                .field("field", "contents")
                .endObject()
                .endObject()
                .endArray()
                .endObject();


        PutPipelineRequest putPipelineRequest = new PutPipelineRequest("ras_attachment", source.bytes(), XContentType.JSON);
        client.admin().cluster().putPipeline(putPipelineRequest).get();


    }





    public static void AddDocuments(){


        Random random_number = new Random();

        long unixSeconds  =  getMeYesterday().getTime() / 1000;


        try {
            BulkRequestBuilder bulkRequest = client.prepareBulk();

            for (int i = 1; i <= 100; i++) {




                String hotelname = "hotel" + i;

                String id_str = Integer.toString(i);
                Integer daily_rate = 1000 + random_number.nextInt(4000);
                if (i==30) {
                    daily_rate += 500000000;
                }
                int star = random_number.nextInt(5);

                String busy_month = Month.randomMonth().toString();
                String closed1 = Day.randomDay().toString();
                String closed2 = Day.randomDay().toString();
                String[] closed = {closed1,closed2};

                String menu1 = FoodMenu.randomFoodMenu().toString();
                String menu2 = FoodMenu.randomFoodMenu().toString();
                String menu3 = FoodMenu.randomFoodMenu().toString();

                String[] menu = {menu1,menu2,menu3};

                String country = Country.randomLocation().toString();
                String japan_city = "";
                if ( country.equals(Country.JAPAN.toString())) {
                    japan_city = JapanCity.randomJapanCity().toString();
                }
                String group = Group.randomGroup().toString();

//                unixSeconds = unixSeconds + 60;
//                Date created = new Date(unixSeconds*1000L); // *1000 is to convert seconds to milliseconds

                int add_date = random_number.nextInt(30);
                int add_date2 = random_number.nextInt(30);
                String created = getCurrentLocalDateTimeStamp(add_date);
                String created2 = getCurrentLocalDateTimeStamp(add_date2);

                String has_parking = i % 2 == 0 ? "yes" : "no";
                String free_text = "this is a test and Hello";
//                String japanese = "紅茶が飲みたい ミルク";
//                japanese = "紅茶飲む ミルク";
//                japanese = "紅茶にミルクを入れる";

                bulkRequest.add(client.prepareIndex(index_name, type_name, id_str).setSource(jsonBuilder()
                                .startObject()
                                .field("hotel_name",hotelname)
                                .field("Account-Name","hello-company")
                                .field("star", star)
                                .field("free_text", free_text)
                                .field("group", group)
                                .field("country", country)
                                .field("city", japan_city)
                                .field("has_parking" , has_parking)
                                .field("menu", menu)
                                .field("busy_month" , busy_month)
                                .field("closed", closed)
                                .field("daily_rate", daily_rate)
                                .field("created", created)
                                .field("created2", created2)

                                .endObject()
                        )
                );



            }

             BulkResponse bulkResponse = bulkRequest.get();

            if (bulkResponse.hasFailures()) {
                // do something

            }



        } catch (Exception e ) {


            System.out.println(e.getStackTrace());
        }
    }


    public static void Delete_By_Query() {
        BulkByScrollResponse response =
                DeleteByQueryAction.INSTANCE.newRequestBuilder(client)
                        .filter(QueryBuilders.matchQuery("country", "FRANCE"))
                        .source(index_name)
                        .size(4000)


                        .get();

        long deleted = response.getDeleted();
    }



    public static void GetDocuments() {


        URL location = Main.class.getProtectionDomain().getCodeSource().getLocation();
        String request_file = location.getFile() + "request.txt";


        for (int i = 1; i <= 5; i++) {
            GetResponse response = client.prepareGet(index_name, type_name,  Integer.toString(i) )
                    .setOperationThreaded(false)
                    .get();

            System.out.println(method_name + " " + index_name + "/" + type_name + "/" + i);
            System.out.println(response.getSourceAsString());
        }

    }




    public static Date getMeYesterday(){
        Object kore = new Date(System.currentTimeMillis());
        return new Date(System.currentTimeMillis()-24*60*60*1000);
    }


    public static String getCurrentLocalDateTimeStamp(int day_to_add) {
        String today =  LocalDateTime.now()
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return LocalDate.parse(today).plusDays(day_to_add).toString();
    }



    public static long getDateDiff() {

        String mirai = getCurrentLocalDateTimeStamp(-4);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate today = LocalDate.now();

        LocalDate today2 = LocalDate.parse(mirai, formatter);
        long days = ChronoUnit.DAYS.between(today, today2);
        return days;
    }




}
