package Util;

import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.elasticsearch.xpack.client.PreBuiltXPackTransportClient;

import java.net.InetAddress;
import java.net.UnknownHostException;



/**
 * Created by surfer on 2017/07/08.
 */
public class EsClient {


    public  static org.elasticsearch.client.Client GetClient(LabEnvironment env, boolean has_xpack) {


        Settings.Builder builder = Settings.builder();

        String user_password = "elastic:changeme";
        builder.put("cluster.name", env.cluster_name);
        if (has_xpack)
            builder.put("xpack.security.user", user_password);


        try {
            TransportAddress transportAddresses = new InetSocketTransportAddress(InetAddress.getByName(env.hostname),9300);

            if (has_xpack) {
                return new PreBuiltXPackTransportClient(builder.build())
                        .addTransportAddress(transportAddresses);
            } else {
                return new PreBuiltTransportClient(builder.build())
                        .addTransportAddress(transportAddresses);
            }


        } catch (UnknownHostException e) {
            e.printStackTrace();

        } finally {

        }

        return null;


    }
}
